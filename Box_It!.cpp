#include<bits/stdc++.h>

using namespace std;

typedef class Box
{
private:
    int length;
    int breadth;
    int height;
public:
    friend bool operator <(Box &box1, Box &box2)
    {
        if (box1.length < box2.length)
            return true;
        if (box1.breadth < box2.breadth && box1.length == box2.length)
            return true;
        if (box1.height < box2.height && box1.length == box2.length &&
            box1.breadth == box2.breadth)
            return true;
        
        return false;
    };
    
    friend ostream& operator <<(ostream& os, const Box &box)
    {
        os << box.length << " " << box.breadth << " " << box.height;
        return os;
    };
    
    Box()
    {
        this->length = 0;
        this->breadth = 0;
        this->height = 0;
    }
    Box(int length, int breadth, int height)
    {
        this->length = length;
        this->breadth = breadth;
        this->height = height;
    }
    Box(Box &box)
    {
        this->length = box.length;
        this->breadth = box.breadth;
        this->height = box.height;
    }
    
    int getLength()
    {
        return this->length;
    }
    int getBreadth()
    {
        return this->breadth;
    }
    int getHeight()
    {
        return this->height;
    }
    long long CalculateVolume()
    {
        long long volume = this->length;
        volume *= this->breadth;
        volume *= this->height;
        return volume;
    }
}Box;


//Implement the class Box  
//l,b,h are integers representing the dimensions of the box

// The class should have the following functions : 

// Constructors: 
// Box();
// Box(int,int,int);
// Box(Box);


// int getLength(); // Return box's length
// int getBreadth (); // Return box's breadth
// int getHeight ();  //Return box's height
// long long CalculateVolume(); // Return the volume of the box

//Overload operator < as specified
//bool operator<(Box& b)

//Overload operator << as specified
//ostream& operator<<(ostream& out, Box& B)


void check2()
{
	int n;
	cin>>n;
	Box temp;
	for(int i=0;i<n;i++)
	{
		int type;
		cin>>type;
		if(type ==1)
		{
			cout<<temp<<endl;
		}
		if(type == 2)
		{
			int l,b,h;
			cin>>l>>b>>h;
			Box NewBox(l,b,h);
			temp=NewBox;
			cout<<temp<<endl;
		}
		if(type==3)
		{
			int l,b,h;
			cin>>l>>b>>h;
			Box NewBox(l,b,h);
			if(NewBox<temp)
			{
				cout<<"smaller\n";
			}
			else
			{
				cout<<"Greater\n";
			}
		}
		if(type==4)
		{
			cout<<temp.CalculateVolume()<<endl;
		}
		if(type==5)
		{
			Box NewBox(temp);
			cout<<NewBox<<endl;
		}

	}
}

int main()
{
	check2();
}