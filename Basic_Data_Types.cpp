    #include <iostream>
    #include <cstdio>
    #include <iomanip>
	#include <cstdlib>

    using namespace std;

    int main()
    {

        int integer;
        long long1;
        long long long2;
        char character;
        float floatnumber;
        double doublenumber;
        
        cout << "Enter your values: "<< endl;
        cin>>integer;
        cin>>long1;
        cin>>long2;
        cin>>character;
        cin>>floatnumber;
        cin>>doublenumber;

        cout << setprecision(10) <<"integer: "<< integer<< endl;
        cout << setprecision(10) << "long1: "<<long1<< endl;
        cout << setprecision(10) << "long2: "<<long2<< endl;
        cout << setprecision(10) << "character: "<<character<< endl;
        cout << setprecision(10) << "floatnumber: "<<floatnumber<< endl;
        cout << setprecision(10) << "doublenumber: "<<doublenumber<< endl;

        return 0;

    }