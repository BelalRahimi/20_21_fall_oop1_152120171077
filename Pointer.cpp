    #include <iostream>
    #include <cmath>
    #include <cstdio>
    #include <cstdlib>
  
    using namespace std;

    void update(int *a,int *b)
    {
        int x = *a;
        *a = *a + *b;
        *b = x - *b;

        if(*b < 0)
     {
            *b = -(*b);
        }
    }

    int main() 
    {
        int a, b;
        
        int *pa = &a, *pb = &b;
        
        cout<<"Enter your values: "<<endl;
        cin>>a>>b;
        
        update(pa, pb);
        
        cout<<"sum: "<<a<<"\n"<<"absoluted difference: "<<b;

        return 0;
    }

