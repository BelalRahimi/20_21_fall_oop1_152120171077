#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream>

using namespace std;
float* myArray;
int length;
string message;

void readFile(string fileName)
{
	string str, str2 = "";
	int counter = 0;
	ifstream file;
	file.open(fileName, ios::in);

	if (!file.is_open())
	{
		message = "There is no specified file.";
		return;
	}

	getline(file, str);

	try
	{
	
		length = stoi(str);

		if (length <= 0)
		{
			message = "Your number is smaller than zero.";
			return;
		}
	}
	catch (const std::exception&)
	{
		message = "Your data is invalid.";
		return;
	}

	myArray = new float[length];
	getline(file, str);

	for (auto x : str)
	{
		if (x == ' ')
		{
			myArray[counter] = stof(str2);
			counter++;
			str2 = "";
		}
		else
			str2 = str2 + x;
	}

	if (length - 1 != counter)
	{
		message = "Invalid number";
		return;
	}
	myArray[counter] = stof(str2);
}

float product(void)
{
	float p = 1;
	for (int i = 0; i < length; i++)
		p *= myArray[i];

	return p;
}
float average(void)
{
	float s = 0;
	for (int i = 0; i < length; i++)
		s += myArray[i];

	return s / length;
}
float sum(void)
{
	float s = 0;
	for (int i = 0; i < length; i++)
		s += myArray[i];

	return s;
}
float smallest(void)
{
	float s = myArray[0];

	for (int i = 0; i < length; i++)
	{
		if (s > myArray[i])
	    	s = myArray[i];
	}
	return s;
}


void main()
{
	readFile("input.txt");

	float pr = product();
	float av = average();
	float su = sum();
	float sm = smallest();
	
	cout << "Sum is	" << su << endl;
	cout << "Product is " << pr << endl;
	cout << "Average is " << av << endl;
	cout << "Smallest is " << sm << endl;
	
	cout << endl;
	system("pause");
}