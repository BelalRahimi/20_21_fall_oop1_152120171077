#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;


int main() 
{
    int size;
    cout<<"Enter size of your array: "<<endl;
    cin >> size;
    
    cout<<"Enter your values: "<<endl;
    int* A = new int[size];
    for (int i = 0; i < size; i++)
        cin >> A[i];

    int* reverse_A = new int[size];
    for (int i = 0; i < size; i++)
        reverse_A[size - i - 1] = A[i];
        
    cout<<"Reverse of your array: "<< endl;
    for (int i = 0; i < size; i++)
        cout <<reverse_A[i] << " ";
    
    return 0;
}


