#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

typedef struct Point
{
    int A;
    int B;
}Point;

int main() 
{
    int n; 
    int q; 

    cin >> n >> q;

    int** matrix = new int*[n];

    int m;
    for (int i = 0; i < n; i++)
    {
        cin >> m;
        matrix[i] = new int[m];
        for (int j = 0; j < m; j++)
        {
            cin >> matrix[i][j];
        }
    }

    Point* points = new Point[q];
    for (int i = 0; i < q; i++)
    {
        Point p;
        cin >> p.A >> p.B;
        points[i] = p;
    }

    for (int i = 0; i < q; i++)
    {
        cout << matrix[points[i].A][points[i].B] << endl;
    }
    
    return 0;
}